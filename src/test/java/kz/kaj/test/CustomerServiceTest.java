package kz.kaj.test;

import kz.kaj.test.domain.Customer;
import kz.kaj.test.dto.CustomerDto;
import kz.kaj.test.dto.base.ResultDto;
import kz.kaj.test.repository.CustomerRepository;
import kz.kaj.test.service.CustomerService;
import kz.kaj.test.service.impl.CustomerServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CustomerServiceTest {

    private final CustomerRepository customerRepository = mock(CustomerRepository.class);
    private final CustomerService customerService = new CustomerServiceImpl(customerRepository);

    @Test
    public void getAllTest() {
        when(customerRepository.findAll()).thenReturn(createListCustomer());

        ResultDto<List<CustomerDto>> resultDto = customerService.getAll();

        Assert.assertNotNull(resultDto.getData());
        Assert.assertEquals(Integer.valueOf(200), resultDto.getCode());
        Assert.assertNull(resultDto.getErrorMessageText());
    }

    private List<Customer> createListCustomer() {
        Customer c = new Customer();
        c.setId(1);
        c.setEmail("test@gmail.com");
        c.setName("test");
        return List.of(c);
    }
}
