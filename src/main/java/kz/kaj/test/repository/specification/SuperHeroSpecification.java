package kz.kaj.test.repository.specification;

import kz.kaj.test.domain.SuperHero;
import org.springframework.data.jpa.domain.Specification;

import java.util.Locale;

public class SuperHeroSpecification {
    public static Specification<SuperHero> genderEquals(String gender) {
        return (root, cq, cb) -> cb.equal(root.get("gender"), gender);
    }

    public static Specification<SuperHero> eyeEquals(String eye) {
        return (root, cq, cb) -> cb.equal(root.get("eye"), eye);
    }

    public static Specification<SuperHero> nameContainsWithIgnoreCase(String name) {
        return (root, cq, cb) -> cb.like(cb.lower(root.get("name").as(String.class)), "%" + name.toLowerCase(Locale.ROOT) + "%");
    }
}
