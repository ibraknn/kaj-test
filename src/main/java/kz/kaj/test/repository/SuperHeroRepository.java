package kz.kaj.test.repository;

import kz.kaj.test.domain.SuperHero;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SuperHeroRepository extends PagingAndSortingRepository<SuperHero, Long>, JpaSpecificationExecutor<SuperHero> {
}
