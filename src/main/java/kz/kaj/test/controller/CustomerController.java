package kz.kaj.test.controller;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import kz.kaj.test.dto.CustomerDto;
import kz.kaj.test.dto.base.ResultDto;
import kz.kaj.test.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static kz.kaj.test.config.Constants.API_V1_REST_ENDPOINT;

@ApiModel(value = "Customer controller")
@RequiredArgsConstructor
@RestController
@RequestMapping(API_V1_REST_ENDPOINT + "customer")
public class CustomerController {
    private final CustomerService customerService;

    @GetMapping("/getAll")
    @ApiOperation(value = "Get all customers", response = CustomerDto.class)
    public ResultDto<List<CustomerDto>> getAll() {
        return customerService.getAll();
    }

    @PostMapping
    @ApiOperation(value = "Create new customer", response = CustomerDto.class)
    public ResultDto<CustomerDto> createCustomer(@ApiParam(value = "Customer dto")
                                                 @Validated @RequestBody CustomerDto dto) {
        return customerService.createCustomer(dto);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update customer", response = CustomerDto.class)
    public ResultDto<CustomerDto> updateCustomer(@ApiParam(value = "Customer dto")
                                                 @Validated @RequestBody CustomerDto dto,
                                                 @ApiParam(value = "Customer id")
                                                 @PathVariable("id") Long id) {
        return customerService.updateCustomer(dto, id);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete customer by id", response = CustomerDto.class)
    public ResultDto<CustomerDto> deleteCustomerById(@ApiParam(value = "Customer id")
                                                     @PathVariable("id") Long id) {
        return customerService.deleteCustomerById(id);
    }
}
