package kz.kaj.test.controller;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import kz.kaj.test.domain.SuperHero;
import kz.kaj.test.dto.SuperHeroesFilterDto;
import kz.kaj.test.service.SuperHeroService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import static kz.kaj.test.config.Constants.API_V1_REST_ENDPOINT;

@ApiModel(value = "SuperHero Controller")
@RequiredArgsConstructor
@RestController
@RequestMapping(API_V1_REST_ENDPOINT + "superhero")
public class SuperHeroController {
    private final SuperHeroService superHeroService;

    @PostMapping("/getAll")
    @ApiOperation(value = "Get all superheroes", response = SuperHero.class)
    public Page<SuperHero> getSuperHeroes(@ApiParam(value = "DTO for pagination")
                                          @RequestBody SuperHeroesFilterDto dto,
                                            Pageable p) {
        return superHeroService.getSuperHeroes(dto, p);
    }

}
