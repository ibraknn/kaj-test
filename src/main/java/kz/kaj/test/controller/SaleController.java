package kz.kaj.test.controller;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import kz.kaj.test.dto.CustomerDto;
import kz.kaj.test.dto.FullOrderDto;
import kz.kaj.test.dto.base.ResultDto;
import kz.kaj.test.service.SaleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static kz.kaj.test.config.Constants.API_V1_REST_ENDPOINT;

@ApiModel(value = "Sale controller")
@RequiredArgsConstructor
@RestController
@RequestMapping(API_V1_REST_ENDPOINT + "sale")
public class SaleController {
    private final SaleService saleService;

    @GetMapping("/getByOrder/{id}")
    @ApiOperation(value = "Get all sale by order id", response = FullOrderDto.class)
    public ResultDto<List<FullOrderDto>> getByOrder(@PathVariable("id") Integer id) {
        return saleService.getByOrder(id);
    }
}
