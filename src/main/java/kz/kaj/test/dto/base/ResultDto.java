package kz.kaj.test.dto.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "ResultDto", description = "Model for response")
public class ResultDto<T> {
    @ApiModelProperty(value = "Status of response")
    @Builder.Default
    private boolean success = false;

    @ApiModelProperty(value = "Error text message")
    private String errorMessageText;

    @ApiModelProperty(value = "Response object")
    private T data;

    @ApiModelProperty(value = "Code status")
    private Integer code;

    public ResultDto(T data) {
        this.data = data;
        this.success = true;
        this.code = 200;
    }
}
