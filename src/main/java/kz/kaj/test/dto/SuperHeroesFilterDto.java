package kz.kaj.test.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "SuperHeroesFilterDto")
public class SuperHeroesFilterDto {

    @ApiModelProperty(notes = "Genger equals filter")
    private String gender;
    @ApiModelProperty(notes = "Eye equals filter")
    private String eye;
    @ApiModelProperty(notes = "Name contains, ignorecase filter")
    private String name;
}
