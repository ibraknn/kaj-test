package kz.kaj.test.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@ApiModel(value = "Full Order dto")
@Getter
@Setter
@NoArgsConstructor
public class FullOrderDto {
    private CustomerDto customerDto;
    private LocalDate orderDate;
    private ProductDto productDto;
    private Integer quantity;
}
