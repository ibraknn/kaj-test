package kz.kaj.test.dto;

import io.swagger.annotations.ApiModel;
import kz.kaj.test.domain.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static java.util.Objects.nonNull;

@ApiModel(value = "Product dto")
@Getter
@Setter
@NoArgsConstructor
public class ProductDto {
    private Integer id;
    private String name;
    private Integer price;
    private String productTypeName;

    public ProductDto(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.price = product.getPrice();
        if (nonNull(product.getProductType())) {
            this.productTypeName = product.getProductType().getTypeName();
        }
    }
}
