package kz.kaj.test.dto;

import io.swagger.annotations.ApiModel;
import kz.kaj.test.domain.SuperHero;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(value = "SuperHero dto")
@Getter
@Setter
@NoArgsConstructor
public class SuperHeroDto {
    private Integer id;
    private String name;
    private String align;
    private String eye;
    private String hair;
    private String gender;
    private Integer appearances;
    private Integer year;
    private String universe;

    public SuperHeroDto(SuperHero superHero) {
        this.id = superHero.getId();
        this.name = superHero.getName();
        this.align = superHero.getAlign();
        this.eye = superHero.getEye();
        this.hair = superHero.getHair();
        this.gender = superHero.getGender();
        this.appearances = superHero.getAppearances();
        this.year = superHero.getYear();
        this.universe = superHero.getUniverse();
    }
}
