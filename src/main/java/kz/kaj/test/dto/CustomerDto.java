package kz.kaj.test.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kz.kaj.test.domain.Customer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@ApiModel(value = "Customer dto")
@Getter
@Setter
@NoArgsConstructor
public class CustomerDto {
    @ApiModelProperty(value = "Customer id")
    private Integer id;
    @ApiModelProperty(value = "Customer name")
    @NotNull
    private String name;
    @ApiModelProperty(value = "Customer email")
    @NotNull
    @Email
    private String email;

    public CustomerDto(Customer customer) {
        this.id = customer.getId();
        this.name = customer.getName();
        this.email = customer.getEmail();
    }
}
