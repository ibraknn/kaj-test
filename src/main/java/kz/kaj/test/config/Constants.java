package kz.kaj.test.config;

public final class Constants {
    public static final String API_V1_REST_ENDPOINT = "/api/v1/";
    private Constants() {
    }
}
