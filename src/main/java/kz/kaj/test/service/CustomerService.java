package kz.kaj.test.service;

import kz.kaj.test.dto.CustomerDto;
import kz.kaj.test.dto.base.ResultDto;

import java.util.List;

public interface CustomerService {
    ResultDto<List<CustomerDto>> getAll();

    ResultDto<CustomerDto> createCustomer(CustomerDto dto);

    ResultDto<CustomerDto> updateCustomer(CustomerDto dto, Long id);

    ResultDto<CustomerDto> deleteCustomerById(Long id);
}
