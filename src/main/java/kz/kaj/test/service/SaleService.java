package kz.kaj.test.service;

import kz.kaj.test.dto.FullOrderDto;
import kz.kaj.test.dto.base.ResultDto;

import java.util.List;

public interface SaleService {
    ResultDto<List<FullOrderDto>> getByOrder(Integer id);
}
