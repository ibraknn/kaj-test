package kz.kaj.test.service.impl;

import kz.kaj.test.domain.SuperHero;
import kz.kaj.test.dto.SuperHeroesFilterDto;
import kz.kaj.test.repository.SuperHeroRepository;
import kz.kaj.test.repository.specification.SuperHeroSpecification;
import kz.kaj.test.service.SuperHeroService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;


import static java.util.Objects.nonNull;

@RequiredArgsConstructor
@Service
public class SuperHeroServiceImpl implements SuperHeroService {
    private final SuperHeroRepository superHeroRepository;

    @Override
    public Page<SuperHero> getSuperHeroes(SuperHeroesFilterDto dto, Pageable p) {
        Specification<SuperHero> specification = null;
        if (nonNull(dto.getGender())) {
            specification = SuperHeroSpecification.genderEquals(dto.getGender());
        }
        if (nonNull(dto.getEye())) {
            specification = specification == null ?
                    SuperHeroSpecification.eyeEquals(dto.getEye()) :
                    specification.and(SuperHeroSpecification.eyeEquals(dto.getEye()));
        }
        if (nonNull(dto.getName())) {
            specification = specification == null ?
                    SuperHeroSpecification.nameContainsWithIgnoreCase(dto.getName()) :
                    specification.and(SuperHeroSpecification.nameContainsWithIgnoreCase(dto.getName()));
        }

        return specification == null ?
                superHeroRepository.findAll(PageRequest.of(p.getPageNumber(), p.getPageSize())) :
                superHeroRepository.findAll(specification, PageRequest.of(p.getPageNumber(), p.getPageSize()));
    }
}
