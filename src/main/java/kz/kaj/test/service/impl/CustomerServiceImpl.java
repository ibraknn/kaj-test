package kz.kaj.test.service.impl;

import kz.kaj.test.domain.Customer;
import kz.kaj.test.dto.CustomerDto;
import kz.kaj.test.dto.base.ResultDto;
import kz.kaj.test.repository.CustomerRepository;
import kz.kaj.test.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    @Override
    public ResultDto<List<CustomerDto>> getAll() {
        List<CustomerDto> dtoList = customerRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(Customer::getId))
                .map(CustomerDto::new)
                .collect(Collectors.toList());
        return new ResultDto<>(dtoList);
    }

    @Override
    public ResultDto<CustomerDto> updateCustomer(CustomerDto dto, Long id) {
        Optional<Customer> customer = customerRepository.findById(id);
        if (customer.isEmpty()) {
            ResultDto<CustomerDto> resultDto = new ResultDto<>();
            resultDto.setErrorMessageText("Does not exist customer with id - " + id);
            resultDto.setCode(404);
            return resultDto;
        }
        return saveCustomer(dto, customer.get());
    }

    private ResultDto<CustomerDto> saveCustomer(CustomerDto dto, Customer customer) {
        customer.setEmail(dto.getEmail());
        customer.setName(dto.getName());
        dto.setId(customerRepository.save(customer).getId());
        return new ResultDto<>(dto);
    }

    @Override
    public ResultDto<CustomerDto> createCustomer(CustomerDto dto) {
        return saveCustomer(dto, new Customer());
    }

    @Override
    public ResultDto<CustomerDto> deleteCustomerById(Long id) {
        Optional<Customer> customer = customerRepository.findById(id);
        if (customer.isEmpty()) {
            ResultDto<CustomerDto> resultDto = new ResultDto<>();
            resultDto.setErrorMessageText("Does not exist customer with id - " + id);
            resultDto.setCode(404);
            return resultDto;
        }

        customerRepository.delete(customer.get());
        return new ResultDto<>(new CustomerDto(customer.get()));
    }
}
