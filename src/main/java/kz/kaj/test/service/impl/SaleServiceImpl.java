package kz.kaj.test.service.impl;

import kz.kaj.test.domain.Sale;
import kz.kaj.test.dto.CustomerDto;
import kz.kaj.test.dto.FullOrderDto;
import kz.kaj.test.dto.ProductDto;
import kz.kaj.test.dto.base.ResultDto;
import kz.kaj.test.repository.SaleRepository;
import kz.kaj.test.service.SaleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@RequiredArgsConstructor
@Service
public class SaleServiceImpl implements SaleService {
    private final SaleRepository saleRepository;

    @Override
    public ResultDto<List<FullOrderDto>> getByOrder(Integer id) {
        List<Sale> saleList = saleRepository.findAllByOrder_Id(id);
        return new ResultDto<>(saleList.stream().map(this::mapToDto).collect(Collectors.toList()));
    }

    private FullOrderDto mapToDto(Sale sale) {
        FullOrderDto dto = new FullOrderDto();
        dto.setOrderDate(sale.getOrder().getDate());
        dto.setCustomerDto(new CustomerDto(sale.getOrder().getCustomer()));
        dto.setQuantity(sale.getQuantity());
        dto.setProductDto(new ProductDto(sale.getProduct()));
        return dto;
    }
}
