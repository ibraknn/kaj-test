package kz.kaj.test.service;

import kz.kaj.test.domain.SuperHero;
import kz.kaj.test.dto.SuperHeroesFilterDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SuperHeroService {
    Page<SuperHero> getSuperHeroes(SuperHeroesFilterDto dto, Pageable p);
}
