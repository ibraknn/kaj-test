package kz.kaj.test.domain;

import kz.kaj.test.domain.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "superheroes")
public class SuperHero extends BaseEntity<Integer> {
    @Column(name = "name")
    private String name;
    @Column(name = "align")
    private String align;
    @Column(name = "eye")
    private String eye;
    @Column(name = "hair")
    private String hair;
    @Column(name = "gender")
    private String gender;
    @Column(name = "appearances")
    private Integer appearances;
    @Column(name = "year")
    private Integer year;
    @Column(name = "universe")
    private String universe;
}
