package kz.kaj.test.domain;

import kz.kaj.test.domain.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Getter
@Setter
@Entity
@Table(name = "product_types")
public class ProductType extends BaseEntity<Integer> {

    @Column(name = "type_name")
    private String typeName;
}
