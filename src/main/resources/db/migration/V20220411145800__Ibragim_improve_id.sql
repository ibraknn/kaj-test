SELECT setval('customers_id_seq', (SELECT MAX(id) FROM customers)+1);
SELECT setval('orders_id_seq', (SELECT MAX(id) FROM orders)+1);
SELECT setval('product_types_id_seq', (SELECT MAX(id) FROM product_types)+1);
SELECT setval('products_id_seq', (SELECT MAX(id) FROM products)+1);
SELECT setval('superheroes_id_seq', (SELECT MAX(id) FROM superheroes)+1);
SELECT setval('sales_id_seq', (SELECT MAX(id) FROM sales)+1);
